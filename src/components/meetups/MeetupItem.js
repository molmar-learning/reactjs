import { useContext } from 'react';

import Card from '../ui/Card';
import classes from './MeetupItem.module.css';
import FavoritesContext from '../../store/favorites-context';

function MeetupItem({id, title, image, address, description, ...props}){

    const favoritesContext = useContext(FavoritesContext)

    const itemIsFavorite = favoritesContext.itemIsFavorite(id)

    function toggleFavoriteStatusHandler(){
        if (itemIsFavorite) {
            favoritesContext.removeFavorite(id)
        } else {
            favoritesContext.addFavorite({
                id: id,
                title: title,
                description: description,
                image: image,
                address: address,
            })
        }
    }

    return (
        <Card>
            <li className={classes.item}>
                <div className={classes.image}>
                    <img src={image} alt={title} />
                </div>
                <div className={classes.content}>
                    <h3>{title}</h3>
                    <address>{address}</address>
                    <p>{description}</p>
                </div>
                <div className={classes.actions}>
                    <button onClick={toggleFavoriteStatusHandler} > { itemIsFavorite ? 'Remove from favorites' : "To favorites" } </button>
                </div>
            </li>
        </Card>
    )
}

export default MeetupItem;